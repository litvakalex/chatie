use rocket::{Rocket, Request, Response, Data};
use rocket::fairing::AdHoc;
use std::collections::HashMap;
use crate::model::user::User;

pub fn mount(mut rock: Rocket, contacts: &HashMap<String, User>) -> Rocket {
    rock = rock.attach(AdHoc::on_attach(
        "** on attach **", 
        on_attach
    ));
    rock = rock.attach(AdHoc::on_request(
        "** on request **", 
        on_request
    ));
    rock = rock.attach(AdHoc::on_response(
        "** on response **", 
        on_response
    ));
    rock
}

fn on_attach(rock: Rocket) -> Result<Rocket, Rocket> {
    debug!("mounting routes...");
    Ok(rock)
}

fn on_request(req: &mut Request, data: &Data) {
    debug!("processing incoming request w (req, data): {:?}", req);
}

fn on_response(req: &Request, res: &mut Response) {
    debug!("processing outgoing request w (req, res): {:?}, {:?}", req, res);
}