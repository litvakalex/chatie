use rocket::Rocket;
use rocket::Request;
use rocket::{catch, catchers};
use std::collections::HashMap;
use crate::model::user::User;

pub fn mount(rock: Rocket, contacts: &HashMap<String, User>) -> Rocket {
    rock.register(
        catchers![
            bad_request, 
            not_found, 
            internal_error
        ]
    )
}

#[catch(400)]
fn bad_request(req: &Request) -> String {
    format!("Bad request '{}'. Try again?", req.uri())
}

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("Not found '{}'.", req.uri())
}

#[catch(500)]
fn internal_error() -> &'static str {
    "Whoops! Sorry, that's on me."
}