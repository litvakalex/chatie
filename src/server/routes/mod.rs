use rocket::Rocket;
use std::collections::HashMap;
use crate::model::user::User;

mod route_home;
mod route_user;

pub fn mount(mut rock: Rocket, contacts: &HashMap<String, User>) -> Rocket {
    rock = route_home::mount(rock);
    rock = route_user::mount(rock, contacts);
    rock
}

