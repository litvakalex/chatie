use rocket::{Rocket};
use rocket::http::{Cookies, RawStr};
use rocket::{get, post, put, routes};
use rocket_contrib::json::Json;

use crate::model::user::User;
use std::collections::HashMap;

pub fn mount(rock: Rocket, contacts: &HashMap<String, User>) -> Rocket {
    rock.mount(
        "/user", 
        routes![
            user_login,
            user_add, 
            user_auth, 
        ]
    )
}

// curl -X GET -b 'id=f2fb4bd5-6495-46d8-a15e-e4f66b0a4bd3' localhost:8000/user
#[get("/")]
fn user_login(mut cookies: Cookies) -> String {
    let id_cookie = cookies
        .get_private("id")
        .map(|c| format!("id: {}", c.value()));
    match id_cookie {
        Some(id_str) => {
            debug!("GET user: {}", id_str);
            id_str
        }
        None => {
            debug!("GET user: None");
            format!("none")
        }
    }
}

// curl -X POST localhost:8000/user/alex
#[post("/<name>")]
fn user_add(name: &RawStr) -> String {
    let name = name.as_str();
    debug!("POST user: {}", name);
    format!("Hello, {}!", name)
}

// curl -X PUT -H 'content-type: application/json' localhost:8000/user \
//  -d '{"id":"f2fb4bd5-6495-46d8-a15e-e4f66b0a4bd3","name":"alex"}'
#[put("/", format = "application/json", data = "<user_json>")]
fn user_auth(user_json: Json<User>) -> Json<User> {
    debug!("PUT userJson: {:?}", user_json);
    let user = user_json.into_inner();
    debug!("PUT user: {:?}", user);
    Json(user)
}