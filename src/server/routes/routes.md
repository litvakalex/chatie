# home, ping

Two basic get request returning string of different type.

## home routes

1. get /

2. get /ping

# user, session, cookie

Users are not permanent, sessions should be saved to local storage
with appropriate user_id, timestamp and all other required properties.
This way sessions can be still be kept for reference, regardless of
whether the _user_ has changed. 

User id can be kept at client as a cookie and reused between sessions.

## auth flow

1. get /user - make a get request to check cookies for existing id

2. post /user - send user name to receive id, init new session and set cookie

3. put /user - refresh auth details for a secure session


