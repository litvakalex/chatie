use rocket::Rocket;
use rocket::{get, routes} ;

pub fn mount(rock: Rocket) -> Rocket {
    rock.mount(
        "/", 
        routes![
            home, 
            ping
        ]
    )
}

// curl -X POST localhost:8000/ping
#[get("/ping")]
fn ping() -> &'static str {
    debug!("GET ping");
    "pong"
}

// curl -X POST localhost:8000
#[get("/")]
fn home() -> String {
    info!("GET home");
    format!("Home")
}