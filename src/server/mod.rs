use rocket::Rocket;

use crate::model::user::User;
use std::collections::HashMap;

mod catchers;
mod fairings;
mod routes;
mod static_routes;

pub fn mount(mut rock: Rocket, contacts: &HashMap<String, User>) -> Rocket {
    rock = catchers::mount(rock, contacts);
    rock = fairings::mount(rock, contacts);
    rock = static_routes::mount(rock, contacts);
    rock = routes::mount(rock, contacts);
    rock
}