use rocket::response::NamedFile;
use rocket::{get, routes, Rocket};
use std::path::{PathBuf, Path};
use std::collections::HashMap;
use crate::model::user::User;

pub fn mount(rock: Rocket, contacts: &HashMap<String, User>) -> Rocket {
    rock.mount(
        "/",
        routes![
            static_asset
        ]
    )
}

#[get("/<file..>")]
fn static_asset(file: PathBuf) -> Option<NamedFile> {
    debug!("GET static: {:?}", file);
    NamedFile::open(Path::new("assets/").join(file)).ok()
}