//
// https://en.wikipedia.org/wiki/Bit_rate#Video
//
// 16 kbit/s – videophone quality (minimum necessary for a consumer-acceptable "talking head")
// 128–384 kbit/s – business-oriented videoconferencing quality using video compression
// 400 kbit/s YouTube 240p videos (using H.264)[21]
// 750 kbit/s YouTube 360p videos (using H.264)[21]
// 1 Mbit/s YouTube 480p videos (using H.264)[21]
// 1.15 Mbit/s max – VCD quality (using MPEG1 compression)[22]
// 2.5 Mbit/s YouTube 720p videos (using H.264)[21]

// use rocket::response::Stream;
// use std::io;

// const CHUNK_SIZE: u64 = 512;

// pub fn read_input() {
//     let response = Stream::chunked(io::stdin(), CHUNK_SIZE);
//     let response_str = response.into();
// }

