#![feature(decl_macro)]
pub mod model;

pub mod server;

pub mod stream;

#[macro_use]
extern crate log;
extern crate chrono;
extern crate fern;

#[cfg(test)]
mod tests;

use std::collections::HashMap;

fn main() {
    match setup_logger() {
        Ok(res) => {
            warn!("logger set, {:?}", res);
        }
        Err(err) => {
            error!("logger setup failed {:?}", err);
        }
    };

    let mut contacts = HashMap::new();
    let mut rock = rocket::ignite();
    rock = server::mount(rock, &contacts);

    rock.launch();
}

fn setup_logger() -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(fern::log_file("output.log")?)
        .apply()?;
    Ok(())
}
