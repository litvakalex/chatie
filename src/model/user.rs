use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct User {
    pub id: String,
    pub name: String,
}

impl User {
    pub fn new(id: &str, name: &str) -> Self {
        User {
            id: String::from(id),
            name: String::from(name),
        }
    }
}
