# **chatie**

## TL;DR

"&==D~... We all see what we will in the clouds", Dildo Bogpelt, Harmonquest

## Chaatiee!

I have been looking into rust, needed motivation to build something. Trying to record a live 'D&D' session with my friends and finding it more and more frustrating as there seem to be no simple ~peer~to~peer~ text-audio-video chat with ability to record a conference locally for every one of the participants. After a little research made this readme and a rocket server with ```/``` and ```/ping``` as a poc and a part of initial commit.

I mean really, pay for the entry-level package to record video? Looking at you Big-Ant-Streaming. Screen fucking capture from external mic on my ultrabook(not really, but bear with me)?! What?! What year is it Marty?! Oh god oh lord, we must have teleported... must have time.. travelled... Because why wouldn't you just record it locally?! On a geek-pimped out android device?! In 2021?.. Ok, but why?

Fortunate ones can already have optical internet _today_. It's crazy and not unlike bitcoin, you better bet on it getting even sweeter. More over and specifically for my purpose which is having a live connection, quality of your connection would depend _heavily_ on the quality of your network. Other factors as well, like torrents running in the background or any thing consuming trafic in some way.

My Galaxy Note 20 has 256GB, so does my Ubuntu Asus Lapdog btw. It's not small, it's normal...
~3hour 4K 60FPS Avatar movie goes for under 50Gs and should read at the speed of 92Mbps accorind to the [wikipedia](https://en.wikipedia.org/wiki/Ultra_HD_Blu-ray). My 100Mbps promissed internet scratches that limit theoretically on the download, but fails miserably on the upload of ~2Mbps.
So I start from here: 

1. text chat - live 'p2p' text chat, connectivity issues, broadcast buffer,

2. in-memory storage - user registration and authentication, _no session persistence_

3. persistent storage - hashed+peppered session, _file_

4. persistent storage - contacts and history, _mongodb_

5. audio chat - Collect Underpants

6. video chat - ?

7. record conference - Profit!

# TODOs


## ACTION server bootstrap, resources being considered

https://outcrawl.com/rust-react-realtime-chat - basic concepts of live chat

https://crates.io/crates/rocket - rust web server 

https://crates.io/crates/rocket-auth-login - user authorization


## BACKLOG 1 client concept and poc

https://outcrawl.com/rust-react-realtime-chat - mentioned in _server bootstrap_, at this point might as well just steal their React.js client


## BACKLOG 2 server infrastructure

https://www.heroku.com/pricing
free cloud computing forever, 
1. VM 

https://circleci.com/product/#hosting-options - can build on local machine?!
